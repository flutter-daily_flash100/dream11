import 'dart:math';

import 'package:flutter/material.dart';

class First extends StatefulWidget {
  const First({super.key});

  @override
  State<First> createState() => _FirstState();
}

class _FirstState extends State<First> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 218, 47, 47),
        actions: <Widget>[
          Row(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(right: 70),
                child: Image.network(
                  "https://i0.wp.com/finmint.com/wp-content/uploads/2018/06/Dream11_Logo_Horizontal_RedBG.png?fit=3097%988&ssl=1",
                ),
              ),
            ],
          ),
          IconButton(
            icon: const Icon(
              Icons.wallet_outlined,
              color: Colors.white,
            ),
            onPressed: () {}, // Replace with your desired functionality
          ),
        ],
        leading: const Icon(
          Icons.notification_add,
          color: Colors.white,
        ),
      ),
      body: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              SizedBox(
                width: 30,
              ),
              Container(
                child: Image.asset("assets/first1.png"),
              ),
              SizedBox(
                width: 45,
              ),
              Container(
                child: Image.asset("assets/second2.png"),
              ),
              SizedBox(
                width: 45,
              ),
              Container(
                child: Image.asset("assets/third3.png"),
              ),
              SizedBox(
                width: 45,
              ),
              Container(
                child: Image.asset("assets/fourth4.png"),
              ),
              SizedBox(
                width: 45,
              ),
              Container(
                child: Image.asset("assets/fifth5.png"),
              ),
            ],
          ),
          Row(
            children: [
              SizedBox(
                width: 30,
              ),
              Container(
                child: Image.asset("assets/1.png"),
              ),
              SizedBox(
                width: 35,
              ),
              Container(
                child: Image.asset("assets/2.png"),
              ),
              SizedBox(
                width: 38,
              ),
              Container(
                child: Image.asset("assets/3.png"),
              ),
              SizedBox(
                width: 40,
              ),
              Container(
                child: Image.asset("assets/4.png"),
              ),
              SizedBox(
                width: 40,
              ),
              Container(
                child: Image.asset("assets/5.png"),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                SizedBox(
                  width: 5,
                ),
                Container(
                  child: Image.asset(
                    "assets/dream.png",
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  child: Image.asset(
                    "assets/dream.png",
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                  child: Image.asset(
                    "assets/dream.png",
                  ),
                ),
              ],
            ),
          ),
          Container(child: Image.asset("assets/text.png")),
          SizedBox(
            height: 18,
          ),
          Expanded(
              child: ListView(
            children: <Widget>[
              Container(
                height: 90,
                width: 390,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5), // Shadow color
                        spreadRadius: 3, // Spread radius
                        blurRadius: 5, // Blur radius
                        offset: Offset(0, 3), // Offset from the container
                      ),
                    ],
                    border: Border.all(
                      color: Color.fromARGB(255, 168, 161,
                          161), // You can set the color of the border here
                      width: 1, // You can set the width of the border here
                    ),
                    color: const Color.fromARGB(255, 255, 253, 253),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.only(right: 100),
                          child: Image.asset("assets/Ellipse 6.png"),
                        ),
                        Container(
                          child: Text(
                            "VS",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w700),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 100),
                          child: Image.asset("assets/Ellipse 7.png"),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 15),
                          child: Text("IND"),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                            child: Icon(
                          Icons.timer_outlined,
                          color: Colors.red,
                        )),
                        Container(
                          child: Text(
                            "01hr 08m 02s",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 65),
                          child: Text("NZ"),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 90,
                width: 390,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5), // Shadow color
                        spreadRadius: 3, // Spread radius
                        blurRadius: 5, // Blur radius
                        offset: Offset(0, 3), // Offset from the container
                      ),
                    ],
                    border: Border.all(
                      color: Color.fromARGB(255, 168, 161,
                          161), // You can set the color of the border here
                      width: 1, // You can set the width of the border here
                    ),
                    color: const Color.fromARGB(255, 255, 253, 253),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.only(right: 100),
                          child: Image.asset("assets/Ellipse 6.png"),
                        ),
                        Container(
                          child: Text(
                            "VS",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w700),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 100),
                          child: Image.asset("assets/Ellipse 7.png"),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 15),
                          child: Text("IND"),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                            child: Icon(
                          Icons.timer_outlined,
                          color: Colors.red,
                        )),
                        Container(
                          child: Text(
                            "01hr 08m 02s",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 65),
                          child: Text("NZ"),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 90,
                width: 390,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5), // Shadow color
                        spreadRadius: 3, // Spread radius
                        blurRadius: 5, // Blur radius
                        offset: Offset(0, 3), // Offset from the container
                      ),
                    ],
                    border: Border.all(
                      color: Color.fromARGB(255, 168, 161,
                          161), // You can set the color of the border here
                      width: 1, // You can set the width of the border here
                    ),
                    color: const Color.fromARGB(255, 255, 253, 253),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.only(right: 100),
                          child: Image.asset("assets/Ellipse 6.png"),
                        ),
                        Container(
                          child: Text(
                            "VS",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w700),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 100),
                          child: Image.asset("assets/Ellipse 7.png"),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 15),
                          child: Text("IND"),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                            child: Icon(
                          Icons.timer_outlined,
                          color: Colors.red,
                        )),
                        Container(
                          child: Text(
                            "01hr 08m 02s",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 65),
                          child: Text("NZ"),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 90,
                width: 390,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5), // Shadow color
                        spreadRadius: 3, // Spread radius
                        blurRadius: 5, // Blur radius
                        offset: Offset(0, 3), // Offset from the container
                      ),
                    ],
                    border: Border.all(
                      color: Color.fromARGB(255, 168, 161,
                          161), // You can set the color of the border here
                      width: 1, // You can set the width of the border here
                    ),
                    color: const Color.fromARGB(255, 255, 253, 253),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.only(right: 100),
                          child: Image.asset("assets/Ellipse 6.png"),
                        ),
                        Container(
                          child: Text(
                            "VS",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w700),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 100),
                          child: Image.asset("assets/Ellipse 7.png"),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 15),
                          child: Text("IND"),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                            child: Icon(
                          Icons.timer_outlined,
                          color: Colors.red,
                        )),
                        Container(
                          child: Text(
                            "01hr 08m 02s",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 65),
                          child: Text("NZ"),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 90,
                width: 390,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5), // Shadow color
                        spreadRadius: 3, // Spread radius
                        blurRadius: 5, // Blur radius
                        offset: Offset(0, 3), // Offset from the container
                      ),
                    ],
                    border: Border.all(
                      color: Color.fromARGB(255, 168, 161,
                          161), // You can set the color of the border here
                      width: 1, // You can set the width of the border here
                    ),
                    color: const Color.fromARGB(255, 255, 253, 253),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.only(right: 100),
                          child: Image.asset("assets/Ellipse 6.png"),
                        ),
                        Container(
                          child: Text(
                            "VS",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w700),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 100),
                          child: Image.asset("assets/Ellipse 7.png"),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 15),
                          child: Text("IND"),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                            child: Icon(
                          Icons.timer_outlined,
                          color: Colors.red,
                        )),
                        Container(
                          child: Text(
                            "01hr 08m 02s",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 65),
                          child: Text("NZ"),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 90,
                width: 390,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5), // Shadow color
                        spreadRadius: 3, // Spread radius
                        blurRadius: 5, // Blur radius
                        offset: Offset(0, 3), // Offset from the container
                      ),
                    ],
                    border: Border.all(
                      color: Color.fromARGB(255, 168, 161,
                          161), // You can set the color of the border here
                      width: 1, // You can set the width of the border here
                    ),
                    color: const Color.fromARGB(255, 255, 253, 253),
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.only(right: 100),
                          child: Image.asset("assets/Ellipse 6.png"),
                        ),
                        Container(
                          child: Text(
                            "VS",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.w700),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 100),
                          child: Image.asset("assets/Ellipse 7.png"),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                          padding: EdgeInsets.only(right: 15),
                          child: Text("IND"),
                        ),
                        SizedBox(
                          width: 50,
                        ),
                        Container(
                            child: Icon(
                          Icons.timer_outlined,
                          color: Colors.red,
                        )),
                        Container(
                          child: Text(
                            "01hr 08m 02s",
                            style: TextStyle(color: Colors.red),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 65),
                          child: Text("NZ"),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),

              // Add more containers here as needed
            ],
          ))
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.red,
        elevation: 9, // Add elevation to create a shadow
        shape: CircleBorder(),
        onPressed: () {},
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Color.fromARGB(255, 233, 51, 51),
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: Colors.white,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            label: 'Create team',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
              color: Colors.white,
            ),
            label: 'Profile',
          ),
        ],
        selectedLabelStyle: TextStyle(color: Colors.white),
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white,
      ),
    );
  }
}
